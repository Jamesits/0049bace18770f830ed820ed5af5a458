#!/bin/bash

# Converts MTR result to InfluxDB line protocol. 
# Intended to be run from telegraf like this:
#
# [[inputs.exec]]
#   commands = ["/path/to/your/mtr_lineprotocol.sh"]
#   interval = "120s"
#   timeout = "120s"
#   data_format = "influx"
#
# Task list format:
# Plain text, one domain/ip per line

TASK_LIST_URL="http://localhost:8000/task.txt"
MTR_AUX_ARGS="--no-dns --report-cycles 10 --gracetime 0.5"
MEASUREMENT="mtr"

set -Eeuo pipefail

download() {
        if command -v curl >/dev/null; then
                curl -fsSL "$1"
                return 0
        fi

        if command -v wget >/dev/null; then
                wget -qO- "$1"
                return 0
        fi

        >&2 echo "No supported download tool, please install either curl or wget."
        return 255
}

mtrline() {
        >&2 echo "> tracing $1"
        # generate a fake header
        RET_CSV_HEADER_STR=$(mtr --csv ${MTR_AUX_ARGS} localhost | head -n 1)
        IFS=',' read -r -a KEYS <<< "${RET_CSV_HEADER_STR}"

        # real data
        RET_CSV=$(mtr --csv ${MTR_AUX_ARGS} "$1" | tail -n +2)
        while IFS= read -r LINE; do
                IFS=',' read -r -a VALUES <<< "${LINE}"
                LP_TAGS=""
                LP_FIELDS=""
                LP_TIMESTAMP=0
                for i in "${!VALUES[@]}"; do
                        KEY="${KEYS[i]}"
                        VALUE=$(printf '%q' "${VALUES[i]}")

                        case $KEY in
                        (Start_Time)
                                LP_TIMESTAMP="${VALUE}"
                                ;;
                        (Mtr_Version) # trash
                                ;;
                        (Host) # tags
                                LP_TAGS="${LP_TAGS}${KEY}=${VALUE},"
                                ;;
                        (Hop|Snt) # integer fields
                                LP_FIELDS="${LP_FIELDS}${KEY}=${VALUE}i,"
                                ;;
                        (*[![:blank:]]*) # other fields
                                LP_FIELDS="${LP_FIELDS}${KEY}=${VALUE},"
                                ;;
                        (*) # all fields with blank keys, trash
                                ;;
                        esac
                done

                # TODO: better data validation
                # - if tags or fields are empty
                echo "${MEASUREMENT},${LP_TAGS%?} ${LP_FIELDS%?} $((LP_TIMESTAMP * 1000000000))"
        done <<< "${RET_CSV}"
}

>&2 echo "MTR to InfluxDB line protocol script by James Swineson <github@public.swineson.me>, 2021-01-27"

if ! command -v mtr >/dev/null; then
        >&2 "Please install mtr first."
        exit 255
fi

TASKS=$(download "${TASK_LIST_URL}")
while IFS= read -r TASK; do
        mtrline "${TASK}"
done <<< "${TASKS}"
